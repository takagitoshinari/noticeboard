<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>ユーザー管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	
	<div class="management">
	<a href="signup">新規登録する</a>
		<table border="3">
			<tr>
				<th>ユーザー名</th>
				<th>支店</th>
				<th>役職</th>
				<th>編集</th>
				<th>停止</th>
			</tr>

			<c:forEach items="${users}" var="user">
				<tr>
					<div class="user-management">
						<span class="name"><td><c:out value="${user.name}" /></td></span>
						<span class="branchName"><td><c:out
									value="${user.branchName}" /></td></span> <span class="departmentName"><td><c:out
									value="${user.departmentName}" /></td></span>
						<td>
							<form action="settings" method="get">
								<button type="submit" name="settingManagement">編集</button>
								<input name="user_id" value="${user.id}" id="user_id"
									type="hidden">
							</form>
						</td>
						<c:choose>
							<c:when test="${user.account == 0}">
								<td>
									<form action="management" method="post">
										<button type="submit" name="account" value="1" onclick = "return confirm('本当に停止してもよろしいですか？')">停止</button>
										<input name="user_id" value="${user.id}" type="hidden">
									</form>
								</td>
							</c:when>
							<c:when test="${user.account == 1}">
								<td>
									<form action="management" method="post">
										<button type="submit" name="account" value="0" onclick = "return confirm('本当に復活してもよろしいですか？')">復活</button>
										<input name="user_id" value="${user.id}" type="hidden">
									</form>
								</td>
							</c:when>
						</c:choose>

					</div>
				</tr>
			</c:forEach>
		</table>
	</div>

</body>
</html>