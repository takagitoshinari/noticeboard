<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.name}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    
        <div class="main-contents">
        
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>
                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name"/>（名前はあなたの公開プロフィールに表示されます）<br />

                <label for="id_login">ログインID</label>
                <input name="id_login" value="${editUser.id_login}" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />
                
                <label for="password">パスワード(確認用)</label>
                <input name="checkPassword" type="password" id="password"/> <br />
                
                <select name="branch_id">
					<option value="1">本社</option>
					<option value="2">支店A</option>
					<option value="3">支店B</option>
					<option value="4">支店C</option>
				</select>　<br />
				<select name="department_id">
					<option value="1">本社総務部</option>
					<option value="2">社員A</option>
					<option value="3">社員B</option>
					<option value="4">社員C</option>
				</select>　<br />

                <input type="submit" value="登録" /> <br />
                <a href="./management">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Your Name</div>
           
        </div>
    </body>
</html>