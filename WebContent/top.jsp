<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>ホーム</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/top.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<br />
			</c:if>
			<c:if test="${ not empty loginUser }">
				<br />
				<div class="fanction">
					<a href="logout">■ ログアウト</a> <br /> <a href="./new.jsp">■ 新規投稿</a>
					<br /> <a href="./management">■ ユーザー管理</a>
				</div>
				<br />
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
				<br />

				<form action="./" method="get">
					<label for="q">● カテゴリー検索：</label> <select name="categorySearch">
						<option value="de" disabled>選んでください</option>
						<option value="study">study</option>
						<option value="help">ヘルプ</option>
						<option value="play">部活動</option>
						<option value="important">important</option>
					</select> <input type="submit" value="検索">
				</form>
				<br />


				<form action="./" method="get">
					<label for="q">● 日付検索：</label> <label><input type="date"
						name="date1">～<input type="date" name="date2"></label> <input
						type="submit" value="検索">
				</form>

				<div class="messages">
					<br />
					<c:forEach items="${messages}" var="message">
						<c:if test="${message.deleted == 0}">
							<div class="message">
								<div class="message-center">
									<div class="title">
										<span class="title">[件名]：<c:out
												value="${message.title}" /></span>
									</div>
									<div class="category">
										<span class="category">[カテゴリー]：<c:out
												value="${message.category}" /></span>
									</div>
									<div class="name">
										<span class="name">[投稿者]：<c:out value="${message.name}" /></span>
									</div>
									<div class="text">
										[本文]：
										<c:out value="${message.text}" />
									</div>
									<div class="date">
										[日付]：
										<fmt:formatDate value="${message.created_date}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>

									<form action="deleteMessage" method="post">
										<div class="delete-button">
											<button type="submit" name="delete" value="1">投稿削除</button>
											<input name="message_id" value="${message.id}" type="hidden">
										</div>
									</form>
								
								<c:if test="${ not empty errorMessages }">
									<div class="errorMessages">
										<ul>
											<c:forEach items="${errorMessages}" var="message">
												<li><c:out value="${message}" />
											</c:forEach>
										</ul>
									</div>
									<c:remove var="errorMessages" scope="session" />
								</c:if>

								<form action="comments" method="post">
									<textarea name="comments" cols="100" rows="5"
										class="comments-box" placeholder ="コメント記入欄"></textarea>
									<input name="message_id" value="${message.id}" id="message_id"
										type="hidden"> <br /> <input type="submit"
										value="コメント送信" class="comment-submit">
								</form>
								
								<div class="comments">
									<c:forEach items="${comments}" var="comment">
										<c:if test="${comment.deleted == 0}">
											<c:if test="${message.id == comment.messageId}">
												<div class="comment">
													<div class="account-name">
														<span class="name">[コメント人]<c:out
																value="${comment.name}" /></span>
													</div>
													<div class="text">
														[コメント文]
														<c:out value="${comment.text}" />
													</div>
													<div class="date">
														[コメント日時]
														<fmt:formatDate value="${comment.created_date}"
															pattern="yyyy/MM/dd HH:mm:ss" />
													</div>
													<form action="deleteComment" method="post">
														<div class="delete-button">
															<button type="submit" name="delete" value="1">コメント削除</button>
															<input name="comment_id" value="${comment.id}"
																type="hidden">
														</div>
													</form>
												</div>
											</c:if>
										</c:if>
									</c:forEach>
								</div>
							</div>
							</div>
							<br />
						</c:if>
					</c:forEach>
				</div>
			</c:if>
		</div>

	</div>
</body>
</html>