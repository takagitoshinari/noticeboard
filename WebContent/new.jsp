<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
	<div class="form-area">
		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
			<br />
			<a href="signup">登録する</a>
		</c:if>
		<c:if test="${ not empty loginUser }">
		<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
			<form action="new" method="post">
				投稿しようぜ！<br /> 
				<input type="text" name="title" size="30"  placeholder="件名">
				<select name="category">
					<option value="">選択してください</option>
					<option value="help">ヘルプ</option>
					<option value="study">勉強</option>
					<option value="play">部活動</option>
					<option value="important">重要事項</option>
				</select>
				<textarea name="message" cols="100" rows="10" class="tweet-box"></textarea>
				<br /> <input type="submit" value="投稿する">
			</form>
		</c:if>
	</div>
</body>
</html>