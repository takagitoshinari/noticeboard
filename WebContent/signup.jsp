<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>新規登録</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<c:if test="${loginUser.branch_id == 1 && loginUser.department_id == 1 }">
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
		<form action="signup" method="post">
			<br /> <label for="name">名前</label> <input name="name" id="name" /><br />
			<label for="id_login">ログインID</label> <input name="id_login"
				id="id_login" /> <br /> <label for="password">パスワード</label> <input
				name="password" type="password" id="password" /> <br />
				<label for="password">パスワード(確認用)</label> <input
				name="checkPassword" type="password" id="password" /> <br />
				<select name="branch_id">
					<option value="1">本社</option>
					<option value="2">支店A</option>
					<option value="3">支店B</option>
					<option value="4">支店C</option>
				</select>
				<select name="department_id">
					<option value="1">本社総務部</option>
					<option value="2">社員A</option>
					<option value="3">社員B</option>
					<option value="4">社員C</option>
				</select>
				 <input
				type="submit" value="登録" /> <br />
				 <a href="./top.jsp">戻る</a>
		</form>
	</div>
	</c:if>
</body>
</html>