package noticeBoard.service;

import static noticeBoard.utils.CloseableUtil.*;
import static noticeBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import noticeBoard.beans.User;
import noticeBoard.dao.UserDao;
import noticeBoard.utils.CipherUtil;
public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User getUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection(); //getConnection()はデータベ＝スの接続を確立する

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, userId);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public List<User> getUserManagement() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> user = userDao.getUserManagementList(connection);

            commit(connection); //データベースの変更を保存

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();
            if(user.getPassword() != null) {
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            }
            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    
    
    public void updateAccount(User account) { //アカウント停止、復活

        Connection connection = null;
        try {
            connection = getConnection();
            
            UserDao userDao = new UserDao();
            userDao.updateAccount(connection, account);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public int checkId(String Id) {

        Connection connection = null;
        try {
            connection = getConnection(); //getConnection()はデータベ＝スの接続を確立する

            UserDao userDao = new UserDao();
            int user = userDao.checkId(connection, Id);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    
}