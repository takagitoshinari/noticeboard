package noticeBoard.service;

import static noticeBoard.utils.CloseableUtil.*;
import static noticeBoard.utils.DBUtil.*;

import java.sql.Connection;

import noticeBoard.beans.User;
import noticeBoard.dao.UserDao;
import noticeBoard.utils.CipherUtil;

public class LoginService {

    public User login(String id_login, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, id_login, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}