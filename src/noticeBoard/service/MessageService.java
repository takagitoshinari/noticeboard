package noticeBoard.service;

import static noticeBoard.utils.CloseableUtil.*;
import static noticeBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import noticeBoard.beans.Message;
import noticeBoard.beans.UserMessage;
import noticeBoard.dao.MessageDao;
import noticeBoard.dao.UserMessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection(); //DBに接続

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public void update(Message deleteMessage) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao userMessageDao = new UserMessageDao();
            userMessageDao.update(connection, deleteMessage);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	
	public List<UserMessage> getUser(String searchCategory) {

        Connection connection = null;
        try {
            connection = getConnection(); //getConnection()はデータベ＝スの接続を確立する

            UserMessageDao userMessageDao = new UserMessageDao();
            List<UserMessage> searchedCategory = userMessageDao.getUser(connection, searchCategory);

            commit(connection);

            return searchedCategory;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	
	
	private static final String CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP";
	public List<UserMessage> getDate(String searchDate1 , String searchDate2) {

        Connection connection = null;
        try {
            connection = getConnection(); //getConnection()はデータベ＝スの接続を確立する

            UserMessageDao userMessageDao = new UserMessageDao();
            List<UserMessage> searchedDate = userMessageDao.getUser(connection, searchDate1,searchDate2, CURRENT_TIMESTAMP);

            commit(connection);

            return searchedDate;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}