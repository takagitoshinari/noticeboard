package noticeBoard.service;

import static noticeBoard.utils.CloseableUtil.*;
import static noticeBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import noticeBoard.beans.Comments;
import noticeBoard.beans.UserComments;
import noticeBoard.dao.CommentsDao;
import noticeBoard.dao.UserCommentsDao;

public class CommentsService {

    public void register(Comments comments) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentsDao commentsDao = new CommentsDao();
            commentsDao.insert(connection, comments);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserComments> getComments() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentsDao commentsDao = new UserCommentsDao();
            List<UserComments> ret = commentsDao.getUserComments(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public void update(UserComments deleteComment) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentsDao userCommentsDao = new UserCommentsDao();
            userCommentsDao.update(connection, deleteComment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}