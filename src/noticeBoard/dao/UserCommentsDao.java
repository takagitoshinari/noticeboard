package noticeBoard.dao;

import static noticeBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import noticeBoard.beans.UserComments;
import noticeBoard.exception.NoRowsUpdatedRuntimeException;
import noticeBoard.exception.SQLRuntimeException;

public class UserCommentsDao {

    public List<UserComments> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.deleted as deleted, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComments> ret = toUserCommentsList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComments> toUserCommentsList(ResultSet rs)
            throws SQLException {

        List<UserComments> ret = new ArrayList<UserComments>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int deleted = rs.getInt("deleted");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                int messageId = rs.getInt("message_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComments comments = new UserComments();
                comments.setName(name);
                comments.setDeleted(deleted);
                comments.setId(id);
                comments.setUserId(userId);
                comments.setMessageId(messageId);
                comments.setText(text);
                comments.setCreated_date(createdDate);

                ret.add(comments);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    
    public void update(Connection connection, UserComments deletedComment) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE comments SET");
            sql.append(" deleted = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");
            
            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, deletedComment.getDeleted());
            ps.setInt(2, deletedComment.getId());
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}