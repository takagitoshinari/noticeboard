package noticeBoard.dao;

import static noticeBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import noticeBoard.beans.User;
import noticeBoard.exception.NoRowsUpdatedRuntimeException;
import noticeBoard.exception.SQLRuntimeException;
public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("id_login");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); //id_login
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // department_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getId_login());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setString(4, user.getBranch_id());
			ps.setString(5, user.getDepartment_id());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String id_login,String password) {

		PreparedStatement ps = null; //pareparedStatementオブジェクト作成（実行したいSQL文の値を？で表現可能）
		try {
			String sql = "SELECT * FROM users WHERE (id_login = ?) AND password = ?";

			ps = connection.prepareStatement(sql);   //pareareStatementクラスのオブジェクトを受け取る
			ps.setString(1, id_login);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	

	private List<User> toUserList(ResultSet rs) throws SQLException { //

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int account = rs.getInt("account");
				String id_login = rs.getString("id_login");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branch_id = rs.getString("branch_id");
				String department_id = rs.getString("department_id");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setId_login(id_login);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setDepartment_id(department_id);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public User getUser(Connection connection, int id) {  //ユーザー編集のとこ

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
	
	public List<User> getUserManagementList(Connection connection) { //getユーザー管理

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id, ");
            sql.append("users.name, ");
            sql.append("users.account, ");
            sql.append("branches.name, ");
            sql.append("departments.name ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");
            sql.append("ORDER BY users.id ASC");
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserManagementList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	
	 private List<User> toUserManagementList(ResultSet rs) //toユーザー管理
	            throws SQLException {
		
		 List<User> ret = new ArrayList<User>();
	        try {
	            while (rs.next()) {
	            	int id = rs.getInt("id");
	                String name = rs.getString("name");
	                int account = rs.getInt("account");
	                String branchName = rs.getString("branches.name");
	                String departmentName = rs.getString("departments.name");
	                User userManagement = new User();
	                userManagement.setId(id);
	                userManagement.setAccount(account);
	                userManagement.setName(name);
	                userManagement.setBranchName(branchName);
	                userManagement.setDepartmentName(departmentName);
	                ret.add(userManagement);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	}
	public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            
            if(user.getPassword() != null) {
	            sql.append(" name = ?");
	            sql.append(", password = ?");
	            sql.append(", id_login = ?");
	            sql.append(", branch_id = ?");
	            sql.append(", department_id = ?");
	            sql.append(", updated_date = CURRENT_TIMESTAMP");
	            sql.append(" WHERE");
	            sql.append(" id = ?");
            }else {
            	sql.append(" name = ?");
                sql.append(", id_login = ?");
                sql.append(", branch_id = ?");
                sql.append(", department_id = ?");
                sql.append(", updated_date = CURRENT_TIMESTAMP");
                sql.append(" WHERE");
                sql.append(" id = ?");
            }
            ps = connection.prepareStatement(sql.toString());

            
            if(user.getPassword() != null) {
	            ps.setString(1, user.getName());
	            ps.setString(2, user.getPassword());
	            ps.setString(3, user.getId_login());
	            ps.setString(4, user.getBranch_id());
	            ps.setString(5, user.getDepartment_id());
	            ps.setInt(6, user.getId());
            }else {
            	ps.setString(1, user.getName());
                ps.setString(2, user.getId_login());
                ps.setString(3, user.getBranch_id());
                ps.setString(4, user.getDepartment_id());
                ps.setInt(5, user.getId());
            }
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	
	
	public void updateAccount(Connection connection, User account) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" account = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");
            
            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, account.getAccount());
            ps.setInt(2, account.getId());
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	
	public int checkId(Connection connection, String id) {//Stringというjavaのクラス

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id_login ");
            sql.append(" FROM users ");
            sql.append(" WHERE users.id_login = ? "  );
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
            int ret = tocheckId(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
	private int tocheckId(ResultSet rs) //toユーザー管理
            throws SQLException {
	 List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {	
                String id_login = rs.getString("id_login");
                User checkId = new User();
                checkId.setId_login(id_login);
                ret.add(checkId);
            }
            return ret.size();
        } finally {
            close(rs);
        }
}
}