package noticeBoard.dao;

import static noticeBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import noticeBoard.beans.Message;
import noticeBoard.beans.UserMessage;
import noticeBoard.exception.NoRowsUpdatedRuntimeException;
import noticeBoard.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("messages.title as title, ");
			sql.append("messages.deleted as deleted, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int deleted = rs.getInt("deleted");
				String text = rs.getString("text");
				String category = rs.getString("category");
				String title = rs.getString("title");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setDeleted(deleted);
				message.setCategory(category);
				message.setTitle(title);
				message.setName(name);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void update(Connection connection, Message deleted) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE messages SET");
			sql.append(" deleted = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, deleted.getDeleted());
			ps.setInt(2, deleted.getId());
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> getUser(Connection connection, String searchCategory) { //ユーザー編集のとこ

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("messages.title as title, ");
			sql.append("messages.deleted as deleted, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE category ");
			sql.append("LIKE ? ");
			sql.append("ORDER BY created_date ASC ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, searchCategory);

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> getUser(Connection connection, String date1, String date2, String CURRENT_TIMESTAMP) { //ユーザー編集のとこ

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("messages.title as title, ");
			sql.append("messages.deleted as deleted, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date ");
			if (!(date1.isEmpty()) && !(date2.isEmpty())) {
				sql.append("BETWEEN ' " + date2 + " 00:00:00 ' " + " AND ' " + date2 + " 23:59:59 '" );
				sql.append(" ORDER BY messages.created_date DESC ");
				ps = connection.prepareStatement(sql.toString());
				ResultSet rs = ps.executeQuery();
				List<UserMessage> ret = toUserMessageList(rs);
				return ret;
				
			} else if (!(date1.isEmpty()) && date2.isEmpty()) {
				sql.append("BETWEEN ' " + date1 + " 00:00:00" + " ' AND " + CURRENT_TIMESTAMP);
				sql.append(" ORDER BY messages.created_date DESC ");
				ps = connection.prepareStatement(sql.toString());
				ResultSet rs = ps.executeQuery();
				System.out.println(ps);
				List<UserMessage> ret = toUserMessageList(rs);
				return ret;
			} else {
				sql.append("BETWEEN ' " + " 1998-11-01 00:00:00 ' " + " AND ' " + date2 + " 23:59:59 " + " ' ");
				sql.append(" ORDER BY messages.created_date DESC ");
				ps = connection.prepareStatement(sql.toString());
				ResultSet rs = ps.executeQuery();
				List<UserMessage> ret = toUserMessageList(rs);
				return ret;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}