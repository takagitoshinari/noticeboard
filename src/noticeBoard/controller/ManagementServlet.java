package noticeBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import noticeBoard.beans.User;
import noticeBoard.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	HttpSession session = request.getSession();
    	List<String> messages = new ArrayList<String>();
        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }
        if(isValid(user, messages) == true) {
        List<User> users = new UserService().getUserManagement();      
        request.setAttribute("users", users);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.getRequestDispatcher("./management.jsp").forward(request, response);
        }else {
        	session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	User account = new User();
    	account.setAccount(Integer.parseInt(request.getParameter("account")));
    	account.setId(Integer.parseInt(request.getParameter("user_id")));
    	 new UserService().updateAccount(account);
    	 response.sendRedirect("./management");
    }
    
private boolean isValid(User user, List<String> messages) {	
        
        if (Integer.parseInt(user.getBranch_id()) != 1 && Integer.parseInt(user.getDepartment_id()) != 1) {
            messages.add("権限がありません");
        }      
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}