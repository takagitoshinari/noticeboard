package noticeBoard.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import noticeBoard.beans.UserComments;
import noticeBoard.service.CommentsService;


@WebServlet(urlPatterns = {"/deleteComment"})
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override //編集情報をPOST
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
			//List<String> messages = new ArrayList<String>(); //エラーメッセージ用の箱を定義しているだけ        
			
			UserComments deleted = new UserComments();
			deleted.setDeleted(Integer.parseInt(request.getParameter("delete")));
			deleted.setId(Integer.parseInt(request.getParameter("comment_id")));
	    	 new CommentsService().update(deleted);
	    	 response.sendRedirect("./");       
    }

    


    
}