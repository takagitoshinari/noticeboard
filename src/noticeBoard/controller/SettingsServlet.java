package noticeBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import noticeBoard.beans.User;
import noticeBoard.exception.NoRowsUpdatedRuntimeException;
import noticeBoard.service.UserService;

@WebServlet("/settings")
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, 
            HttpServletResponse response) throws ServletException, IOException {

        //ユーザー情報のidを元にDBからユーザー情報取得(DAOでidを条件にSQL文でカラムを全取得する)
        User settingManaUser = new UserService().getUser(Integer.parseInt( request.getParameter("user_id")));
        request.setAttribute("editUser", settingManaUser);
        
      //dogetメソッドもしくはdopostメソッドから抜けた後、指定したURLが表示される。↓
        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

	@Override //編集情報をPOST
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();        
        HttpSession session = request.getSession();
        
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {
            try  {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }
            session.setAttribute("loginUser", editUser);
            response.sendRedirect("./management");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request) //ユーザーが編集した内容をbeansにセット
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name")); 
        if(request.getParameter("password").isEmpty() == false) {
        editUser.setPassword(request.getParameter("password"));
        }
        editUser.setId_login(request.getParameter("id_login"));
        editUser.setBranch_id(request.getParameter("branch_id"));
        editUser.setDepartment_id(request.getParameter("department_id"));
        return editUser; //beansにセットしたらリターンしている
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String id_login = request.getParameter("id_login");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String checkPassword = request.getParameter("checkPassword");
        String branch_id = request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");

        if (StringUtils.isEmpty(id_login) == true) {
            messages.add("ログインIDを入力してください");
        }
        
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        
        if (password.contains(checkPassword) == false) {
            messages.add("同じパスワードが入力されていません");
        }
        
        if (StringUtils.isEmpty(branch_id) == true) {
            messages.add("支店名を入力してください");
        }
        
        if (StringUtils.isEmpty(department_id) == true) {
            messages.add("役職を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}