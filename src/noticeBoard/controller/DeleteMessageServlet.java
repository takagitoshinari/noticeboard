package noticeBoard.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import noticeBoard.beans.Message;
import noticeBoard.service.MessageService;


@WebServlet(urlPatterns = {"/deleteMessage"})
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override //編集情報をPOST
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
			//List<String> messages = new ArrayList<String>(); //エラーメッセージ用の箱を定義しているだけ        
			
			Message deleted = new Message();
			deleted.setDeleted(Integer.parseInt(request.getParameter("delete")));
			deleted.setId(Integer.parseInt(request.getParameter("message_id")));
	    	 new MessageService().update(deleted);
	    	 response.sendRedirect("./");       
    }

    


    
}