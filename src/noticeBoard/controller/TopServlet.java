package noticeBoard.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import noticeBoard.beans.User;
import noticeBoard.beans.UserComments;
import noticeBoard.beans.UserMessage;
import noticeBoard.service.CommentsService;
import noticeBoard.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        List<UserMessage> messages = new MessageService().getMessage();
        List<UserComments> comments = new CommentsService().getComments();
        
        if(request.getParameter("categorySearch") != null) {//カテゴリー検索パラメーター確認
        	List<UserMessage> searchedCategory = new MessageService().getUser( request.getParameter("categorySearch"));
            request.setAttribute("messages", searchedCategory);
            request.setAttribute("comments", comments);
            request.getRequestDispatcher("/top.jsp").forward(request, response);
        }else if(request.getParameter("date1") == "" && request.getParameter("date2") =="") {
        	request.setAttribute("messages", messages);
            request.setAttribute("comments", comments);
            request.getRequestDispatcher("/top.jsp").forward(request, response);
        }else if(request.getParameter("date1") != null || request.getParameter("date2") != null){
        	List<UserMessage> searchedDate = new MessageService().getDate(request.getParameter("date1"), request.getParameter("date2"));
        	request.setAttribute("messages", searchedDate);
            request.setAttribute("comments", comments);
            request.getRequestDispatcher("/top.jsp").forward(request, response);
        }else {
        	request.setAttribute("messages", messages);
            request.setAttribute("comments", comments);
            request.setAttribute("isShowMessageForm", isShowMessageForm);
            request.getRequestDispatcher("/top.jsp").forward(request, response);
        }
         
        
    }
    
    
}