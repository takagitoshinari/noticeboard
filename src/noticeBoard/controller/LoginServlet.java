package noticeBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import noticeBoard.beans.User;
import noticeBoard.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String id_login = request.getParameter("id_login");
        String password = request.getParameter("password");
        
        LoginService loginService = new LoginService();
        User user = loginService.login(id_login, password);
        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();
        if (user != null  ) { 
        	if(user.getAccount() == 0) {
        		session.setAttribute("loginUser", user);
                response.sendRedirect("./");
        	}else {
        		messages.add("アカウントが停止されています。");
        		session.setAttribute("errorMessages", messages);
        		response.sendRedirect("./login.jsp");
        	} 
        } else {
            messages.add("ログインに失敗しました。");
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("login");
        }
    }

}