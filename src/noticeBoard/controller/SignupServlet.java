package noticeBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import noticeBoard.beans.User;
import noticeBoard.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        
        if (isValid(request, messages) == true) {
            User user = new User();
            user.setId_login(request.getParameter("id_login"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(request.getParameter("branch_id"));
            user.setDepartment_id(request.getParameter("department_id"));
            new UserService().register(user);
            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String id_login = request.getParameter("id_login");
    	String password = request.getParameter("password");
    	String checkPassword = request.getParameter("checkPassword");
    	String name = request.getParameter("name");
        String branch = request.getParameter("branch_id");
        String department = request.getParameter("department_id");
        
        int	idLogin = new UserService().checkId(request.getParameter("id_login"));
        
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (10 < name.length()) {
            messages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isEmpty(id_login) == true) {
            messages.add("ログインIDを入力してください");
        }
        if(idLogin != 0) {
        	messages.add("このログインIDはすでに使用されています。");
        }
        if (id_login.matches("^[0-9a-zA-Z]+${6,20}") == false) {
            messages.add("ログインIDは半角英数字で6文字以上20文字以下にして下さい");
        }
       
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (password.matches("^[ -/:-@\\[-\\`\\{-\\~a-zA-Z0-9]+${6,20}") == false ) {
            messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下にして下さい。");
        }

        if (password.contains(checkPassword) == false) {
            messages.add("同じパスワードが入力されていません");
        }
        
        if (StringUtils.isEmpty(branch) == true) {
            messages.add("支店を入力してください");
        }
        if (StringUtils.isEmpty(department) == true) {
            messages.add("役職をを入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}