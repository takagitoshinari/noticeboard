package noticeBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import noticeBoard.beans.Message;
import noticeBoard.beans.User;
import noticeBoard.service.MessageService;

@WebServlet(urlPatterns = { "/new" })
public class NewServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();
        if (isValid(request, messages) == true) {
        	
            User user = (User) session.getAttribute("loginUser");
            Message message = new Message();
            message.setText(request.getParameter("message"));
            message.setTitle(request.getParameter("title"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());
            
            new MessageService().register(message);
            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./new.jsp");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	
    	String title = request.getParameter("title");
        String message = request.getParameter("message");

        
        if (StringUtils.isEmpty(title) == true) {
            messages.add("件名を入力してください");
        }
        System.out.println(title);
        if (30 < title.length()) {
            messages.add("件名は30文字以下で入力してください");
        }
        if (StringUtils.isEmpty(message) == true) {
            messages.add("メッセージを入力してください");
        }
        if (1000 < message.length()) {
            messages.add("メッセージは1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}